import java.util.*;

public class Classroom {
    private ClassName className;
    private String classNumber;
    private Set<Student> studentSet = new HashSet<>();
    private Map<SubjectType, SchoolSubject> subjectMap = new HashMap<>();

    public Classroom(ClassName className) {
        this.className = className;
        this.classNumber = classNumber;
    }

    public ClassName getClassName() {
        return className;
    }

    public String getClassNumber() {
        return classNumber;
    }

    public Set<Student> getStudentSet() {
        return studentSet;
    }

    public void addStudent(String pesel, String name, String surname){
        studentSet.add(new Student(pesel, name, surname));
    }

    public void addSubject(SubjectType subjectType, SchoolSubject schoolSubject){
        subjectMap.put(subjectType, schoolSubject);
    }

    public void printStudents(){
        List<Student> studentList = new ArrayList<>();
        studentList.addAll(studentSet);
        studentList.sort(new Comparator<Student>() {
            public int compare(Student o1, Student o2) {
                return 0;
            }
        });

        System.out.println("Studenci: ");
        for (Student student : studentList){
            System.out.println(student);
        }
    }

    public void addGrade (Integer studentID, SubjectType subjectType, Grade grade){
        System.out.println("Dodaję ocenę " + grade.toString() + " uczniowi o nr " + studentID + ", z przedmiotu " + subjectType.toString().toLowerCase());
        subjectMap.get(subjectType).addGrade(studentID, grade);
    }
}