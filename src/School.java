import java.util.HashMap;
import java.util.Map;

public class School {
    private Map<ClassName, Classroom> classMap = new HashMap<>();

    public void addClassroom(Classroom classroom) {
        classMap.put(classroom.getClassName(), classroom);
    }

    public boolean doesClassExist(String classNumber) {
        boolean checker = false;
        for (Classroom classroom : classMap.values()) {
            if (classroom.getClassNumber().equals(classNumber)) {
                checker = true;
            }
        }
        return checker;
    }

    public void printClassStudents(String classNumber) {
        boolean flag = false;
        for (Classroom classroom : classMap.values()) {
            if (classroom.getClassNumber().equals(classNumber)) {
                flag = true;
                for (Student student : classroom.getStudentSet()) {
                    System.out.println(student.toString());
                }
            }
        }
        if(!flag){
            System.out.println("Nie znaleziono klasy o podanej nazwie.");
        }
    }
}