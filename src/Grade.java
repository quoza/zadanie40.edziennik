public class Grade {
    private float grade;
    private GradeType gradeType;

    public Grade(float grade, GradeType gradeType) {
        this.grade = grade;
        this.gradeType = gradeType;
    }

    public float getGrade() {
        return grade;
    }

    public void setGrade(float grade) {
        this.grade = grade;
    }

    public GradeType getGradeType() {
        return gradeType;
    }

    public void setGradeType(GradeType gradeType) {
        this.gradeType = gradeType;
    }

    @Override
    public String toString() {
        return "Grade {" +
                "grade = " + grade +
                ", gradeType = " + gradeType +
                '}';
    }
}
