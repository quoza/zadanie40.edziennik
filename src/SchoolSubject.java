import java.util.*;

public class SchoolSubject {
    private Map<Integer, List<Grade>> studentGrades = new HashMap<>();

    public SchoolSubject(int studentNumber) {
        for (int i = 0; i < studentNumber; i++) {
            studentGrades.put(i, new LinkedList<>());
        }
    }

    public void addGrade(Integer studentID, Grade grade) {
        System.out.println("Dodaje ocene " + grade.getGrade() + " uczniowi " + studentID + ".");
        if (studentGrades.containsKey(studentID)) {
            studentGrades.get(studentID).add(grade);
        } else {
            studentGrades.put(studentID, new ArrayList<Grade>());
            studentGrades.get(studentID).add(grade);
        }
    }

    public void printGradesAllStudens(){
        System.out.println("Drukuje oceny wszystkich uczniow.");
        for (Map.Entry<Integer, List<Grade>> grades : studentGrades.entrySet()){
            System.out.println("ID ucznia: " + grades.getKey() + " => " + grades.getValue());
        }
    }

    public void printGradesOneStudent(Integer studentID) {
        if (studentGrades.containsKey(studentID)) {
            System.out.println("ID ucznia: " + studentID + " => " + studentGrades.get(studentID));
        } else{
            System.out.println("Nie znaleziono ucznia.");
        }
    }
}